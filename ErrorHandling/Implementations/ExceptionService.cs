﻿using ErrorHandling.CustomErrors;
using ErrorHandling.ErrorTypes;
using ErrorHandling.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;

namespace ErrorHandling.Implementations
{
    public class ExceptionService : IExceptionService
    {
        private readonly ILogger _logger;
        public ExceptionService(ILogger<ExceptionService> logger)
        {
            _logger = logger;
        }

        public bool GetException(ExceptionTypes exceptionType)
        {
            switch (exceptionType)
            {
                case ExceptionTypes.ARGUMENT_EXCEPTION: return GetArgumentException(); break;
                case ExceptionTypes.FILE_NOT_FOUND_EXCEPTION: return GetFileNotFoundException(); break;
                case ExceptionTypes.ARITHMETIC_EXCEPTION: return GetArithmeticException(); break;
                case ExceptionTypes.BUSINESS_EXCEPTION: return GetBussinesException(); break;
            }

            return true;
        }

        private bool GetArgumentException()
        {
            bool success = true;
            try
            {
                // Datetime constructor max year is 9999
                new DateTime(100000, 1, 1);
            }
            catch (ArgumentOutOfRangeException exception)
            {
                _logger.LogError(exception, "DateTime constructor max date is 9999-12-31");
                success = false;
            }

            return success;
        }
        private bool GetFileNotFoundException()
        {
            bool success = true;
            try
            {
                using (FileStream fileStream = File.OpenRead("./non_existing_file"))
                {
                    // won't reach this point as the file doesn't exists

                    // file existence should be validated before of trying to open it (doing like this only to get exception)
                    // if (File.Exists(path))
                    // {
                    //    File.Delete(path);
                    // }

                }
            }
            catch (FileNotFoundException exception)
            {
                _logger.LogError(exception, "File not found");
                success = false;
            }

            return success;
        }
        private bool GetArithmeticException()
        {
            bool success = true;

            try
            {
                int zero = 0;
                int one = 1;
                var res = one / zero;
            }
            catch (DivideByZeroException exception)
            {
                _logger.LogError(exception, "Encountered divsion by zero");
                success = false;
            }

            return success;
        }


        // NOTE: A good use of custom excpetions is when we work with third party services
        //       those services usually return codes to identify errors
        private bool GetBussinesException()
        {
            bool success = true;

            try
            {
                // Assuming duty start is after duty finish 
                var startOfDuty = new DateTime(2021, 4, 4, 20, 0, 0);
                var endOfDuty = new DateTime(2021, 4, 4, 10, 0, 0);

                if (startOfDuty > endOfDuty)
                    throw new BusinessException("Duty start can not be greatter than duty end");
            }
            catch (BusinessException exception)
            {
                _logger.LogError(exception, exception.Message);
                success = false;
            }

            return success;
        }
    }
}
