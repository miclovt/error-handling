﻿using ErrorHandling.ErrorTypes;

namespace ErrorHandling.Interfaces
{
    public interface IExceptionService
    {
        bool GetException(ExceptionTypes exceptionType);
    }
}
