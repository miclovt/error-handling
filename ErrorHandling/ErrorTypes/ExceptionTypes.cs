﻿namespace ErrorHandling.ErrorTypes
{
    // Most Common Exceptions
    public enum ExceptionTypes
    {
        // C# Exceptions
        FILE_NOT_FOUND_EXCEPTION = 1,
        ARGUMENT_EXCEPTION = 2,
        ARITHMETIC_EXCEPTION = 3,

        // Custom Exceptions
        BUSINESS_EXCEPTION = 4
    }
}
