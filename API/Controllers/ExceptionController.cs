﻿using ErrorHandling.ErrorTypes;
using ErrorHandling.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExceptionController : ControllerBase
    {
        private readonly IExceptionService _exceptionService;
        public ExceptionController(IExceptionService exceptionService)
        {
            _exceptionService = exceptionService;
        }

        [HttpGet]
        public IActionResult GetExceptionLogged(ExceptionTypes exceptionType)
        {

            var result = _exceptionService.GetException(exceptionType);
            if (!result)
            {
                // could be more specific in the message but i did not have imagination when write this XD
                return StatusCode(500, $"Something falied {exceptionType.ToString()}");
            }

            return Ok(); 
        }

        [HttpGet("OutOfMemory")]
        public IActionResult GetOutOfMemoryException()
        {
            try
            {
                List<byte[]> wastedMemory = new List<byte[]>();

                while (true)
                {
                    byte[] buffer = new byte[4096]; // Allocate 4kb
                    wastedMemory.Add(buffer);
                }
                // this will never happen
                return Ok();
            }
            catch (OutOfMemoryException e)
            {
                Environment.FailFast($"Out of Memory: {e.Message}");
                return null;
            }
        }
    }
}
